# Importar la conexión a la base de datos
from conexiondb import conexion_db


# CRUD  =>  Create - Read - Update - Delete


# Create =>
def insertar_datos(nombre, cantidad, categoria, precio):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        # sql = "INSERT INTO productos53(id, nombre, cantidad, categoria, precio) VALUES(NULL, 'Jabón', 120, 'Aseo', 2000)"
        sql = f"INSERT INTO productos53(id, nombre, cantidad, categoria, precio) VALUES(NULL, '{nombre}', {cantidad}, '{categoria}', {precio})"
        cursor.execute(sql)
        conexion.commit()
    conexion.close()
    return "Datos guardados"


# Read =>
# leer todos los datos de la tabla
def leer_datos():
    conexion = conexion_db()
    rstDB = []
    # cursor = conexion.cursor()
    with conexion.cursor() as cursor:
        sql = "SELECT * FROM productos53"
        # sql = "SELECT * FROM productos"
        cursor.execute(sql)
        # rstDB = cursor.fetchone()
        rstDB = cursor.fetchall()
    cursor.close()
    return rstDB

# leer único producto referente al id
def leer_datos_id(id):
    conexion = conexion_db()
    rstDB = []
    # cursor = conexion.cursor()
    with conexion.cursor() as cursor:
        sql = f"SELECT * FROM productos53 WHERE id = {id}"
        # sql = "SELECT * FROM productos53"
        # sql = "SELECT * FROM productos"
        cursor.execute(sql)
        rstDB = cursor.fetchone()
        # rstDB = cursor.fetchall()
    cursor.close()
    return rstDB


# Update =>
def actualizar_datos(id, nombre, cantidad, categoria, precio):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        # sql = "UPDATE productos53 SET nombre = 'Aceite', cantidad = 60, categoria = 'Víveres', precio = 6000 WHERE id = 7"
        sql = f"UPDATE productos53 SET nombre = '{nombre}', cantidad = {cantidad}, categoria = '{categoria}', precio = {precio} WHERE id = {id}"
        cursor.execute(sql)
        conexion.commit()
    cursor.close()
    return "Datos actualizados..."


# Delete => 
def eliminar_datos(id):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        # sql = "DELETE FROM productos53 WHERE id = 9"
        sql = f"DELETE FROM productos53 WHERE id = {id}"
        cursor.execute(sql)
        conexion.commit()
    conexion.close()
    return "Datos eliminados..."

