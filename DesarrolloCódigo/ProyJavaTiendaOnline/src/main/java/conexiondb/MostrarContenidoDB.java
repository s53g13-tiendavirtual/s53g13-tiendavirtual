package conexiondb;

import jakarta.servlet.annotation.*;
import jakarta.servlet.http.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "nombreAsignar", value = "/otro-nombre")
public class MostrarContenidoDB extends HttpServlet {
    private String message;


    public void init() {
        message = "Tripulantes Página 2!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");


        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        for (int i = 0; i < 6; i++) {
            out.println("<h" + (i+1) + ">" + "Título h" + (i+1) + ", Variable i = " + i + "</h" + (i+1) + ">");
        }
        out.println("</body></html>");

    }

    public void destroy() {
    }
}
