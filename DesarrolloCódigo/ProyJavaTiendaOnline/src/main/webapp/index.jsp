<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title>
            JSP - Hello World
        </title>
    </head>
    <body>
        <br>
        <h1>
            <%= "Hello World!" %>
        </h1>
        <br>
        <h2>
            Hola Tripulantes - Buen inicio de semana
        </h2>
        <p>
            HTML (Lenguaje de Marcas de Hipertexto,
            del inglés HyperText Markup Language) es el componente
            más básico de la Web. Define el significado
            y la estructura del contenido web.
        </p>
        <br/>
        <a href="hello-servlet">Hello Servlet</a>
        <br>
        <a href="otro-nombre">Link de la otra página</a>
    </body>
</html>